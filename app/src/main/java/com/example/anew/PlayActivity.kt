package com.example.anew

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView

class PlayActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var button1: Button
    private lateinit var button2: Button
    private lateinit var button3: Button
    private lateinit var button4: Button
    private lateinit var button5: Button
    private lateinit var button6: Button
    private lateinit var button7: Button
    private lateinit var button8: Button
    private lateinit var button9: Button

    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()
    private var activePlayer = 1

    private var scoreX: Int = 0 // ქულები
    private var score0: Int = 0

    private lateinit var xWins: TextView
    private lateinit var oWins: TextView

    private var winnerPlayer: Int = 0 // ეს check-იდან გადმოვიტანე აქ

    private lateinit var line: TextView
    private lateinit var turnText: TextView
    private lateinit var resetButton: Button
    private lateinit var restartButton: Button

    private lateinit var mediaPlayer: MediaPlayer // მოგებისთვის

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)

        mediaPlayer = MediaPlayer.create(this, R.raw.voice)

        xWins = findViewById(R.id.xWins)
        oWins = findViewById(R.id.oWins)

        turnText = findViewById(R.id.turnText)
        line = findViewById(R.id.line)

        resetButton = findViewById(R.id.resetButton) // რესეთი
        resetButton.setOnClickListener {
            reset()
        }

        restartButton = findViewById(R.id.restartButton) // რესტარტი
        restartButton.setOnClickListener {

            reset()
            score0 = 0
            scoreX = 0
            xWins.text = "X - 0"
            oWins.text = "O - 0"

        }

        init()

    }

    private fun init(){

        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)

        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

    }

    override fun onClick(clickedview: View?) {

        if (clickedview is Button) {

            var buttonNumber = 0

            when (clickedview.id) {

                R.id.button1 -> buttonNumber = 1
                R.id.button2 -> buttonNumber = 2
                R.id.button3 -> buttonNumber = 3
                R.id.button4 -> buttonNumber = 4
                R.id.button5 -> buttonNumber = 5
                R.id.button6 -> buttonNumber = 6
                R.id.button7 -> buttonNumber = 7
                R.id.button8 -> buttonNumber = 8
                R.id.button9 -> buttonNumber = 9
            }

            if (buttonNumber != 0) {
                playGame(clickedview, buttonNumber)
            }

        }

    }

    private fun playGame(clickedView: Button, buttonNumber: Int) {

        if (activePlayer == 1) {

            clickedView.text = "X"
            activePlayer = 2
            firstPlayer.add(buttonNumber)
            turnText.text = "Turn 0"

        } else {

            clickedView.text = "0"
            activePlayer = 1
            secondPlayer.add(buttonNumber)
            turnText.text = "Turn X"

        }

        clickedView.isEnabled = false
        check()

    }

    /*
        გადახაზვისთვის ერთი ხაზი გავაკეთე და კოორდინატებს ვუცვლი სხვადასხვა ხაზის მოგების დროს :)
        მთლად რესფონსივი არ უნდა იყოს მაგრამ წავა
    */

    private fun check() {

        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            line.translationX = 0f
            line.translationY = -button1.layoutParams.width.toFloat()
            line.rotation = 0.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            line.translationX = 0f
            line.translationY = -button1.layoutParams.width.toFloat()
            line.rotation = 0.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 0.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(4) && secondPlayer.contains(5) && secondPlayer.contains(6)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 0.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)) {
            line.translationX = 0f
            line.translationY = button1.layoutParams.width.toFloat()
            line.rotation = 0.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(7) && secondPlayer.contains(8) && secondPlayer.contains(9)) {
            line.translationX = 0f
            line.translationY = button1.layoutParams.width.toFloat()
            line.rotation = 0.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)) {
            line.translationX = -button1.layoutParams.width.toFloat()
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(4) && secondPlayer.contains(7)) {
            line.translationX = -button1.layoutParams.width.toFloat()
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(2) && secondPlayer.contains(5) && secondPlayer.contains(8)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)) {
            line.translationX = button1.layoutParams.width.toFloat()
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(6) && secondPlayer.contains(9)) {
            line.translationX = button1.layoutParams.width.toFloat()
            line.translationY = 0f
            line.rotation = 90.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 45.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(5) && secondPlayer.contains(9)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = 45.0f
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(5) && firstPlayer.contains(7)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = -45.0f
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(5) && secondPlayer.contains(7)) {
            line.translationX = 0f
            line.translationY = 0f
            line.rotation = -45.0f
            winnerPlayer = 2
        }

        if (winnerPlayer == 1) {
            turnText.text = "Winner - X"
            win()
        } else if (winnerPlayer == 2) {
            turnText.text = "Winner - 0"
            win()
        } else if (firstPlayer.size + secondPlayer.size == 9) {

            turnText.text = "Draw"

            Handler().postDelayed(
                {
                    reset()
                }, 1000
            )

        }

    }

    private fun win() {

        mediaPlayer.start() // ეს ხმა მოგების დროს :D

        button1.isEnabled = false
        button2.isEnabled = false
        button3.isEnabled = false
        button4.isEnabled = false
        button5.isEnabled = false
        button6.isEnabled = false
        button7.isEnabled = false
        button8.isEnabled = false
        button9.isEnabled = false

        line.visibility = View.VISIBLE // გადახაზვა

        Handler().postDelayed(
            {
                if (winnerPlayer == 1) {
                    scoreX++
                } else if (winnerPlayer == 2) {
                    score0++
                }

                line.visibility = View.INVISIBLE

                xWins.text = "X - $scoreX"
                oWins.text = "O - $score0"

                reset()

            }, 1000
        )

    }

    private fun reset() {

        button1.text = ""
        button2.text = ""
        button3.text = ""
        button4.text = ""
        button5.text = ""
        button6.text = ""
        button7.text = ""
        button8.text = ""
        button9.text = ""

        firstPlayer.clear()
        secondPlayer.clear()

        winnerPlayer = 0
        activePlayer = 1

        turnText.text = "Turn X"

        button1.isEnabled = true
        button2.isEnabled = true
        button3.isEnabled = true
        button4.isEnabled = true
        button5.isEnabled = true
        button6.isEnabled = true
        button7.isEnabled = true
        button8.isEnabled = true
        button9.isEnabled = true

    }

    /* ამოტრიალების დროს უკვე ჩაწერილი მნიშვნელობები რო არ წაიშალოს */

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        outState.putString("turnText", turnText.text.toString())

        outState.putInt("activePlayer", activePlayer)

        outState.putInt("winsX", scoreX)
        outState.putInt("wins0", score0)

        outState.putString("oWins", oWins.text.toString())
        outState.putString("xWins", xWins.text.toString())

        outState.putString("btn1", button1.text.toString())
        outState.putString("btn2", button2.text.toString())
        outState.putString("btn3", button3.text.toString())
        outState.putString("btn4", button4.text.toString())
        outState.putString("btn5", button5.text.toString())
        outState.putString("btn6", button6.text.toString())
        outState.putString("btn7", button7.text.toString())
        outState.putString("btn8", button8.text.toString())
        outState.putString("btn9", button9.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

        super.onRestoreInstanceState(savedInstanceState)

        turnText.text = savedInstanceState.getString("turnText")

        activePlayer = savedInstanceState.getInt("activePlayer")

        scoreX = savedInstanceState.getInt("winsX")
        score0 = savedInstanceState.getInt("wins0")

        oWins.text = savedInstanceState.getString("oWins")
        xWins.text = savedInstanceState.getString("xWins")

        button1.text = savedInstanceState.getString("btn1")
        button2.text = savedInstanceState.getString("btn2")
        button3.text = savedInstanceState.getString("btn3")
        button4.text = savedInstanceState.getString("btn4")
        button5.text = savedInstanceState.getString("btn5")
        button6.text = savedInstanceState.getString("btn6")
        button7.text = savedInstanceState.getString("btn7")
        button8.text = savedInstanceState.getString("btn8")
        button9.text = savedInstanceState.getString("btn9")

    }

}